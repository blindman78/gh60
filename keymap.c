#include "satan.h"
#include "keymap_french.h"

#define BASE    0    // layer par defaut
#define SPACE   1    // layer bar d'espace
#define NUMPAD  2    // layer pavé numerique
#define FN1     4    // layer controle led

#define MAC0 M(0) //
#define MAC1 M(1) //
#define MAC2 M(2) //
#define MAC3 M(3) //
#define MAC4 M(4) //
#define MAC5 M(5) //
#define MAC6 M(6) //
#define MAC7 M(7) //
#define MAC8 M(8) //
#define MAC9 M(9) //
#define GRAV KC_GRV //
#define MEDI F(FNO1)//

// General shortenings
#define ESCA KC_ESC
#define MINS KC_MINS
#define EQUL KC_EQL
#define BSPC KC_BSPC
#define DELE KC_DEL
#define LBRC KC_LBRC
#define RBRC KC_RBRC
#define ALTR KC_RALT
#define SCLN KC_SCLN
#define QUOT KC_QUOT
#define NUHS KC_NUHS
#define ENTE KC_ENT
#define NUBS KC_NUBS  // Less/ greater sign
#define COMM KC_COMM  // Comma
#define FSTO KC_DOT   // Full stop
#define SLSH KC_SLSH
#define ALTL KC_LALT
#define GUIL KC_LGUI
#define GUIR KC_RGUI
#define MENO KC_MENU

// Special Actions and Media Keys
#define INSE KC_INS   // Insert here
#define HOME KC_HOME  // Go to beginning of line
#define ENDI  KC_END  // go to end of line
#define PSCR  KC_PSCR   // Print Screen
#define SLCK  KC_SLCK   // go to end of line
#define PGDN  KC_PGDN   // go to end of line
#define PGUP  KC_PGUP   // go to end of line
#define PLPS KC_MPLY  // Play/Pause
#define PAUS KC_PAUS  // Pause button
#define MUTE KC_MUTE  // Mute sound
#define VOLU KC_VOLU  // Volume increase
#define VOLD KC_VOLD  // Volume decrease
#define MNXT KC_MNXT  // next track
#define MPRV KC_MPRV  // prev track
#define MSTP KC_MSTP  // stop playing
#define MSEL KC_MSEL  // Select media (Start playing it)
#define MAIL KC_MAIL  // Open default mail app
#define CALC KC_CALC  // Open default calculator app
#define MYCM KC_MYCM  // Open default file manager

// increase readability
#define _______  KC_TRNS
#define XXXXX    KC_NO

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Keymap BASE: (Base Layer) Default Layer
    ,-----------------------------------------------------------.
    |Esc~| &|  é|  "|  '|  (|  -|  è|  _|  ç|  à|  )|  =|Backsp |
    |-----------------------------------------------------------|
    |Tab  |  A|  Z|  E|  R|  T|  Y|  U|  I|  O|  P|  ^|  $|  R  |
    |-----------------------------------------------------------|
    |CAPS   |  Q|  S|  D|  F|  G|  H|  J|  K|  L|  M|  ù| * | R |
    |-----------------------------------------------------------|
    |Shft| < |  W|  X|  C|  V|  B|  N|  ,|  ;|  :|  !|Shift     |
    |-----------------------------------------------------------|
    |Ctrl|Gui |Alt |      Space            |Alt |Gui |FN  |Ctrl |
    `-----------------------------------------------------------|
*/

[ BASE ] = KEYMAP_ISO_SPLITRSHIFT(
  KC_ESC,         FR_AMP,         FR_EACU,FR_QUOT,FR_APOS,FR_LPRN,FR_MINS,FR_EGRV,FR_UNDS,FR_CCED,FR_AGRV,   FR_RPRN,  FR_EQL,     KC_BSPC, \
  KC_TAB,         FR_A,              FR_Z,   KC_E,   KC_R,   KC_T,   KC_Y,   KC_U,   KC_I,   KC_O,   KC_P,   FR_CIRC, FR_DLR,     XXXXX,    \
  KC_CAPS,        FR_Q,              KC_S,   KC_D,   KC_F,   KC_G,   KC_H,   KC_J,   KC_K,   KC_L,   FR_M,   FR_UGRV, FR_ASTR,     KC_ENT,  \
  KC_LSFT,        KC_NUBS,           FR_W,   KC_X,   KC_C,   KC_V,   KC_B,   KC_N,   FR_COMM,   FR_SCLN,FR_COLN, FR_EXLM, KC_RSFT,     XXXXX, \
  KC_LCTL,        KC_LGUI,KC_LALT,                LT(SPACE, KC_SPC),                     KC_RALT,OSL(FN1),TG(NUMPAD),    KC_RCTL),

[ SPACE ] = KEYMAP(
        KC_GRAVE, KC_F1,  KC_F2,    KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_DEL, XXXXX,\
        _______, _______, _______, _______, MAIL, _______, _______, HOME, KC_UP, PSCR, SLCK, PAUS, PGUP, _______,\
        _______, _______, _______, PGUP   , PGDN, _______, LALT(KC_F4), KC_LEFT, KC_DOWN, KC_RIGHT, _______, _______, _______, _______,\
        _______, _______, _______, _______, CALC, _______, _______, _______, MUTE, VOLD, VOLU, _______, _______, XXXXX,\
        _______, _______, _______,           _______,           _______, _______, _______, _______),

[ NUMPAD ] = KEYMAP( //Numpad and alt shortcuts
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, XXXXX,\
        _______, _______, _______, _______, _______, _______,  _______,KC_P7, KC_P8, KC_P9, KC_PAST, KC_PSLS, KC_EQL, _______,\
        _______, _______, _______, _______, _______, _______,  _______,KC_P4, KC_P5, KC_P6, KC_PMNS, KC_KP_0, KC_PDOT, _______,\
        _______, _______, _______, _______, _______, _______, _______,  _______,KC_P1, KC_P2, KC_P3, KC_PPLS, _______, XXXXX,\
        _______, _______, _______,           _______,             _______, _______, _______, _______),

[ FN1 ] = KEYMAP( //Functions/settings
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,\
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,\
        _______, _______, _______, _______, _______, _______, _______, BL_TOGG, BL_INC, BL_DEC, BL_TOGG, _______, _______, _______,\
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, XXXXX,\
        _______, _______, _______,           _______,           _______, _______, _______, _______),
};
